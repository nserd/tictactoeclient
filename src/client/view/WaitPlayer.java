package client.view;

import client.TicTacToeClient;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class WaitPlayer extends JFrame{
    public boolean close;

    private JLabel l;

    public WaitPlayer(){
        super();

        setMinimumSize(new Dimension(200,45));
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        setUndecorated(true);

        int w = (TicTacToeClient.menu.getWidth() / 2)  - (getWidth() / 2);
        int h = (TicTacToeClient.menu.getHeight() / 2) - (getHeight() / 2);

        setLocation(TicTacToeClient.menu.getX() + w, TicTacToeClient.menu.getY() + h);

        JButton exit = new JButton(new ImageIcon(getClass().getResource(TicTacToeClient.path + "exit.png")));

        exit.addActionListener(new ActionListener() {
            @Override
            public synchronized void actionPerformed(ActionEvent actionEvent) {
                close = true;
                dispose();
            }
        });

        exit.setPreferredSize(new Dimension(10,10));
        exit.setBorderPainted(false);
        exit.setRolloverIcon(new ImageIcon(getClass().getResource(TicTacToeClient.path + "exitInv.png")));

        l = new JLabel("Wait another player");
        l.setFont(new Font("Times New Roman", Font.ITALIC, 15));
        l.setForeground(Color.black);

        JPanel p = new JPanel();

        p.setBackground(Color.white);
        p.setBorder(BorderFactory.createMatteBorder(5,1,5,1, Color.gray));

        p.add(l);
        p.add(exit);

        Container c = getContentPane();
        c.add(p);

        new Thread(new Gif()).start();

        setVisible(true);
    }

    class Gif extends Thread{
        @Override
        public void run() {
            for(int i = 1; !close; i++){
                l.setIcon(new ImageIcon(getClass().getResource(TicTacToeClient.path + "hourglass/frame" + i + ".png")));
                l.repaint();

                if( i == 22)
                    i = 1;

                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
