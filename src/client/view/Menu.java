package client.view;

import client.TicTacToeClient;

import java.awt.*;
import java.awt.event.*;
import java.io.FileInputStream;
import java.io.InputStream;

import javax.swing.*;

public class Menu extends JFrame implements WindowListener{
    private static final long serialVersionUID = 1L;

    private JList<String> tables;
    private DefaultListModel<String> dlm;

    public String out = "";

    private final Object lock = new Object();

    public Menu(){
        super("MENU");
        setSize(220, 300);
        setLocationRelativeTo(null);
        setResizable(false);

        addWindowListener(this);

        initialization();

        out = "GET_TABLES";

        setVisible(true);
    }

    private void initialization(){
        setNameWindow();

        dlm = new DefaultListModel<>();
        tables = new JList<>(dlm);

        tables.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent mouseEvent) {
                if (!tables.isSelectionEmpty())
                    out ="ENTER_TABLE " + tables.getSelectedValue();
            }
            @Override
            public void mousePressed(MouseEvent mouseEvent) {}
            @Override
            public void mouseReleased(MouseEvent mouseEvent) {}
            @Override
            public void mouseEntered(MouseEvent mouseEvent) {}
            @Override
            public void mouseExited(MouseEvent mouseEvent) {}
        });

        JScrollPane scrollPane = new JScrollPane(tables);

        JPanel mainPanel = new JPanel(new BorderLayout());
        JPanel titleAndPlay = new JPanel(new BorderLayout());
        JPanel playButton = new JPanel();
        JPanel listButtons = new JPanel();
        JPanel listPanel1 = new JPanel(new BorderLayout());
        JPanel listPanel2 = new JPanel();

        JLabel title = new JLabel();
        JLabel aTables = new JLabel(" Available tables:");

        JButton play = new JButton(new ImageIcon(getClass().getResource(TicTacToeClient.path + "play.png")));
        JButton ref = new JButton(new ImageIcon(getClass().getResource(TicTacToeClient.path + "refresh.png")));
        JButton createTable = new JButton("Create new table");

        scrollPane.setPreferredSize(new Dimension(210, 150));
        ref.setPreferredSize(new Dimension(25,25));
        play.setPreferredSize(new Dimension(60, 25));

        title.setIcon(new ImageIcon(getClass().getResource(TicTacToeClient.path + "title.png")));

        aTables.setFont(new Font("Times New Roman", Font.BOLD, 15));

        listPanel1.add(aTables, BorderLayout.NORTH);
        listPanel1.add(scrollPane , BorderLayout.SOUTH);

        play.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {out = "PLAY";}
        });

        createTable.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {out = "CREATE_NEW_TABLE";}
        });

        ref.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {out = "GET_TABLES";}
        });

        playButton.add(play);

        titleAndPlay.add(title, BorderLayout.NORTH);
        titleAndPlay.add(playButton, BorderLayout.SOUTH);

        listPanel2.add(listPanel1);

        listButtons.add(createTable);
        listButtons.add(ref);

        mainPanel.add(titleAndPlay, BorderLayout.NORTH);
        mainPanel.add(listPanel2, BorderLayout.CENTER);
        mainPanel.add(listButtons, BorderLayout.SOUTH);

        Container c = getContentPane();
        c.add(mainPanel);
    }

    private void setNameWindow(){
        String name;

        boolean flag;

        while(true){
            try{
                name = JOptionPane.showInputDialog(null,
                        "Enter you name:", null, JComponent.UNDEFINED_CONDITION).trim();

                flag = !name.contains(" ");

                if(name.length() != 0 && flag) {
                    TicTacToeClient.name = name;
                    break;
                }

                else
                    JOptionPane.showMessageDialog(null, "Wrong name. Try again.",
                                                  null, JOptionPane.WARNING_MESSAGE);

            }catch(NullPointerException npe){
                System.exit(0);
            }
        }
    }

    public void getErrorMessage(String string){
        JOptionPane.showMessageDialog(null, string, null, JOptionPane.ERROR_MESSAGE);
    }

    public void setTables(String[] s){
        synchronized (lock) {
            dlm.removeAllElements();

            if (s != null)
                for (String value : s) dlm.addElement(value);

            revalidate();
        }
    }

    public void refresh(){
        out = "GET_TABLES";
    }

    @Override
    public void windowClosing(WindowEvent windowEvent) {
        System.exit(0);
    }

    @Override
    public void windowOpened(WindowEvent windowEvent) {}
    @Override
    public void windowClosed(WindowEvent windowEvent) {}
    @Override
    public void windowIconified(WindowEvent windowEvent) {}
    @Override
    public void windowDeiconified(WindowEvent windowEvent) {}
    @Override
    public void windowActivated(WindowEvent windowEvent) {}
    @Override
    public void windowDeactivated(WindowEvent windowEvent) {}
}
