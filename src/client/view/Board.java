package client.view;

import client.TicTacToeClient;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import javax.swing.*;

public class Board extends JFrame implements WindowListener {
	private JButton [][] tableButton;
	private JPanel board;
    private JPanel score;

    public short id;

    private JLabel p1Name;
    private JLabel p2Name;

    private JLabel p1Score;
    private JLabel p2Score;

    private JLabel info;

    private int p1WinCounter;
    private int p2WinCounter;

	private Icon x = new ImageIcon(getClass().getResource(TicTacToeClient.path + "x.png"));
	private Icon o = new ImageIcon(getClass().getResource(TicTacToeClient.path + "o.png"));
	private Icon empty = new ImageIcon(getClass().getResource(TicTacToeClient.path + "empty.png"));

    private Icon xScore = new ImageIcon(getClass().getResource(TicTacToeClient.path + "x_for_score.png"));
    private Icon oScore = new ImageIcon(getClass().getResource(TicTacToeClient.path + "o_for_score.png"));

	public String coordinates = "";
	public String out = "";

	private int[][] table;

	public Board(){
		super("Tic-tac-toe");

        setMinimumSize(new Dimension(310,410));
        setPreferredSize(new Dimension(310,410));

		setLocationRelativeTo(null);
		setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);

		addWindowListener(this);

		p1WinCounter = 0;
		p2WinCounter = 0;

		Font infoFont = new Font("Consolas", Font.PLAIN, 12);

        info = new JLabel();
		score = new JPanel(new GridLayout(1,2));
        board = new JPanel(new GridLayout(10,10));

        info.setFont(infoFont);

        scoreInitialization();
		boardInitialization();

        JPanel mainPanel = new JPanel(new BorderLayout());
        JPanel boardPanel = new JPanel();
        JScrollPane scrollPane = new JScrollPane(board);

        boardPanel.setMaximumSize(new Dimension(350,350));
        boardPanel.add(scrollPane);

		mainPanel.add(score, BorderLayout.NORTH);
		mainPanel.add(boardPanel, BorderLayout.CENTER);
		mainPanel.add(info, BorderLayout.SOUTH);

        Container c = getContentPane();
        c.add(mainPanel);

        setVisible(true);
	}

    public synchronized void setPlayersName(String p1, String p2){
        p1Name.setText(p1);
        p2Name.setText(p2);
    }

    public synchronized void setInfo(boolean flag){
        if(flag){
            if(!info.getText().equals("You turn.")) {
                info.setText("You turn.");
                info.repaint();
            }
        }

        if(!flag){
            if(!info.getText().equals("Move of the opponent...")) {
                info.setText("Move of the opponent...");
                info.repaint();
            }
        }
    }

    private void scoreInitialization(){
        p1Name = new JLabel();
        p2Name = new JLabel();

        JPanel player1 = new JPanel(new BorderLayout());
        JPanel player2 = new JPanel(new BorderLayout());

        Font fontScore = new Font("Verdana", Font.BOLD, 40);
        Font fontName = new Font("TimesRoman", Font.BOLD, 18);

        out = "GET_NAMES";

        player1.setBorder(BorderFactory.createMatteBorder(1,2,5,1, Color.gray));
        player2.setBorder(BorderFactory.createMatteBorder(1,1,5,2, Color.gray));

        p1Score = new JLabel(p1WinCounter + "");
        p2Score = new JLabel(p2WinCounter + "");

        p1Score.setFont(fontScore);
        p2Score.setFont(fontScore);

        p1Name.setFont(fontName);
        p2Name.setFont(fontName);

        p1Name.setForeground(Color.black);
        p2Name.setForeground(Color.black);

        p1Score.setHorizontalAlignment(JLabel.CENTER);
        p2Score.setHorizontalAlignment(JLabel.CENTER);

        p1Name.setIcon(xScore);
        p2Name.setIcon(oScore);

        player1.add(p1Name, BorderLayout.NORTH);
        player1.add(p1Score, BorderLayout.SOUTH);

        player2.add(p2Name, BorderLayout.NORTH);
        player2.add(p2Score, BorderLayout.SOUTH);

        score.add(player1);
        score.add(player2);
    }

	private void boardInitialization(){
		tableButton = new JButton[10][10];

		board.setPreferredSize(new Dimension(300,300));

		table = new int[10][10];
		
		for(int i = 0; i < table.length; i++)
			for(int j = 0; j < table.length; j++)
				table[i][j] = 0;
			
		for(int i = 0; i < tableButton.length; i++){
			for(int j = 0; j < tableButton.length; j++){
				
				if(table[i][j] == 0)
					tableButton[i][j] = new JButton(empty);
				if(table[i][j] == 1)
					tableButton[i][j] = new JButton(x);
				if(table[i][j] == 2)
					tableButton[i][j] = new JButton(o);
				
				tableButton[i][j].addActionListener(new MyActionListener());
				tableButton[i][j].setActionCommand(i + " " +j) ;
				board.add(tableButton[i][j]);
			}
		}
	}
	
	public void refresh(int[][] t){
		board.removeAll();
		table = t;
		for(int i = 0; i < tableButton.length; i++){
			for(int j = 0; j < tableButton.length; j++){
				
				if(table[i][j] == 0)
					tableButton[i][j] = new JButton(empty);
				if(table[i][j] == 1)
					tableButton[i][j] = new JButton(x);
				if(table[i][j] == 2)
					tableButton[i][j] = new JButton(o);
				
				tableButton[i][j].addActionListener(new MyActionListener());
				tableButton[i][j].setActionCommand(i + " " +j) ;
				board.add(tableButton[i][j]);
			}
		}
		revalidate();
	}

	private void updateScore(short win){
            if (win == 1 && id == 1) {
                p1WinCounter++;
                p1Score.setText("" + p1WinCounter);
            }

            if (win == 1 && id == 2) {
                p2WinCounter++;
                p2Score.setText("" + p2WinCounter);
            }

            if (win == 2 && id == 1) {
                p2WinCounter++;
                p2Score.setText("" + p2WinCounter);
            }

            if (win == 2 && id == 2) {
                p1WinCounter++;
                p1Score.setText("" + p1WinCounter);
            }

            score.revalidate();
    }

    public void switchIcons(){
        if(p1Name.getIcon() == xScore){
            p1Name.setIcon(oScore);
            p2Name.setIcon(xScore);
        }
        else{
            p1Name.setIcon(xScore);
            p2Name.setIcon(oScore);
        }

        score.revalidate();
    }

	public synchronized void notification(short win){
	    updateScore(win);

        Thread r = new Thread(new RematchThread(win));
        r.start();
    }

	public void enemyExit(String name){
        JOptionPane.showMessageDialog(null, name + " disconnected.");
	}

	class MyActionListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
				coordinates =  "# " + e.getActionCommand();
		}
	}

    @Override
    public void windowClosing(WindowEvent windowEvent) {
        int result = JOptionPane.showConfirmDialog(null, "Are you sure you want to quit the game?",
                null, JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE);

        if(result == JOptionPane.YES_OPTION){
            TicTacToeClient.menu.setVisible(true);
            dispose();

            out = "STOP";
        }
    }

	@Override
	public void windowOpened(WindowEvent windowEvent) {
        Dimension current = new Dimension(getSize());
        Dimension max = new Dimension(getMaximumSize());

        if(current.height > max.height && current.getWidth() > max.width){
            setSize(getMaximumSize());

            revalidate();
        }

        if(current.height > max.height || current.getWidth() > max.width){
            if(current.height > max.height){
                setSize(current.width, max.height);
            }
            if(current.getWidth() > max.width){
                setSize(max.width, current.height);
            }

            revalidate();
        }
    }

	@Override
	public void windowClosed(WindowEvent windowEvent) {}
	@Override
	public void windowIconified(WindowEvent windowEvent) {}
	@Override
	public void windowDeiconified(WindowEvent windowEvent) {}
	@Override
	public void windowActivated(WindowEvent windowEvent) {}
	@Override
	public void windowDeactivated(WindowEvent windowEvent) {}

	class RematchThread extends Thread{
        Rematch rm;
	    short win;
	    boolean flag = true;

	    RematchThread(short win){
	        this.win = win;

	        rm = new Rematch(win);
        }

	    public void run() {
            while(flag) synchronized (this){
                if(!TicTacToeClient.start){
                    flag = false;

                    rm.dispose();
                }

                if(rm.out.length() != 0){
                    out = rm.out;
                    rm.out = "";

                    rm.dispose();
                }
            }
        }
    }
}
