package client;

import client.view.Board;
import client.view.Menu;
import client.view.WaitPlayer;

import javax.swing.*;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.net.SocketException;

public class TicTacToeClient extends Thread {
    private static Socket clientSocket;

	public static Board board;
	public static Menu menu;
	private static WaitPlayer wait;

    public static boolean start;
	private static boolean setName;
    private static boolean isWait;
    private static boolean turn;

	public static String name;
    public static String path;

	private static final Object lock1 = new Object();
	private static final Object lock2 = new Object();

	public static void main(String args[]) {
        path = "/client/img/";
        String ip = JOptionPane.showInputDialog(null, "Enter IP:");

        menu = new Menu();

		try {
			clientSocket = new Socket(ip, 11111);
		} catch (IOException e) {
			menu.getErrorMessage("Incorrect address or server is down.");
			System.exit(-1);
		}

		Thread t = new Thread(new TicTacToeClient());
		t.start();

		waitAnswer();
	}

	private static void waitAnswer() {
		synchronized (lock1) {
			try {
				BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
				StringBuilder message;

                //noinspection InfiniteLoopStatement
                while (true) {
					message = new StringBuilder("");
					char c;

					while (in.ready()) {
						c = (char) in.read();
						if (c == '!')
							break;

						message.append(c);
					}

					if (message.length() != 0)
						commands(message.toString());
				}
			} catch (SocketException se) {
                menu.getErrorMessage("Server is not available.");
				System.exit(-1);
			} catch (IOException ioe) {
				ioe.printStackTrace();
			}
		}
	}

	private synchronized static void commands(String message) {
        // $ - MENU
        // @ - BOARD
        // # - BOARD.TABLE

	    /* GAME_START
	     *
	     * # ...
	     *
	     * @EXIT
	     * @NAMES
	     * @REMATCH
	     * @YOU_TURN
	     * @WAIT_TURN
	     * @WIN
	     * @LOSE
	     * @DRAW
	     *
	     * $ERROR
	     * $NO_TABLES
	     * $ ...
	     *
	     * WAIT
	     */

		if (message.equals("GAME_START")) {
			board = new Board();
            menu.setVisible(false);
            menu.setEnabled(true);

			if(isWait) {
                wait.dispose();

                isWait = false;
            }
			start = true;
		}

		else if (message.charAt(0) == '#') {
			String[] buff = message.split(" ", 2);
			String[] tableString = buff[1].split("\n");

			int[][] table = new int[10][10];

			for (int i = 0; i < table.length; i++) {
				String[] elements = tableString[i].split(" ");
				for (int j = 0; j < table.length; j++) {
					table[i][j] = Integer.parseInt(elements[j]);
				}
			}
			board.refresh(table);

            if(board.id == 1){
                board.setInfo(true);
            }
            else{
                board.setInfo(false);
            }

            turn = !turn;
            board.setInfo(turn);
		}

		else if (message.charAt(0) == '@') {
            if (message.contains("EXIT")){
                start = false;

                String[] buff = message.split(" ");

                board.enemyExit(buff[1]);
                board.dispose();

                menu.setVisible(true);
                menu.refresh();
            }

            if (message.contains("NAMES")){
                String[] buff = message.split(" ");

                board.setPlayersName(buff[1], buff[2]);
                board.id = (short)Integer.parseInt(buff[3]);

                if(board.id == 1){
                    turn = true;
                    board.setInfo(true);
                }
                else{
                    turn = false;
                    board.setInfo(false);
                }
            }

			if(message.contains("REMATCH")){
                String[] buff = message.split(" ");

                if(buff[1].equals("false")){
                    if(board.id == 1){
                        turn = true;
                        board.setInfo(true);
                    }
                    else{
                        turn = false;
                        board.setInfo(false);
                    }
                }
                else{
                    if(board.id == 1){
                        turn = false;
                        board.setInfo(false);
                    }
                    else{
                        turn = true;
                        board.setInfo(true);
                    }
                }

                board.switchIcons();
            }

            if(message.contains("YOU_TURN")){
			    board.setInfo(true);
            }
            if(message.contains("WAIT_TURN")){
                board.setInfo(false);
            }

            if (message.contains("WIN")) {
                board.notification((short)1);
            }

			if (message.contains("LOSE")) {
				board.notification((short)2);
			}

			if(message.contains("DRAW")){
                board.notification((short)0);
            }
        }

		else if (message.charAt(0) == '$') {
            if (message.contains("ERROR")){
                menu.getErrorMessage("Table full or doesn't exist");
                menu.refresh();
            }
            else {
                if (message.contains("NO_TABLES")) {
                    menu.setTables(null);
                } else {
                    String[] buff = message.split(" ", 2);
                    String[] data = buff[1].split("\n");

                    menu.setTables(data);
                }

            }
		}

		else if (message.equals("WAIT")) {
            menu.setEnabled(false);

			wait = new WaitPlayer();
            isWait = true;
		}
	}

	private static void sendMessage() {
		try {
			DataOutputStream outToServer = new DataOutputStream(clientSocket.getOutputStream());

            //noinspection InfiniteLoopStatement
            while (true) synchronized (lock2) {
                if (!setName)
                    if (name.length() != 0) {
                        outToServer.writeUTF("NAME " + name + '\n');

                        setName = true;
                    }

                if (start) {
                    if (board.coordinates.length() != 0) {
                        outToServer.writeBytes(board.coordinates + '\n');

                        board.coordinates = "";
                    }

                    if (board.out.length() != 0) {
                        outToServer.writeBytes(board.out + '\n');

                        if (board.out.equals("STOP"))
                            start = false;

                        board.out = "";
                    }
                }

                if (menu.out.length() != 0) {
                    outToServer.writeBytes(menu.out + '\n');

                    menu.out = "";
                }

                if (isWait) {
                    if (wait.close) {
                        outToServer.writeBytes("DELETE" + '\n');

                        wait.close = false;
                        menu.setEnabled(true);
                    }
                }
            }
		} catch (SocketException se) {
			menu.getErrorMessage("Server is not available.");
			System.exit(0);
		} catch (IOException ioe) {
			ioe.printStackTrace();
		}
	}

	public void run() {
		sendMessage();
	}
}